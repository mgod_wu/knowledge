# 如何共建开发样例

丰富多样的OpenHarmony开发样例离不开广大合作伙伴和开发者的贡献，如果你也想把自己开发的样例分享出来，欢迎把样例提交到OpenHarmony-SIG仓来，优秀的样例我们也会把它发布到OpenHarmony官网，将展示你的大名哦，最有代表性的样例，还有机会被合入OpenHarmony主干哦。为了保持统一的风格，我们统一了各个仓库的目录结构。

## 样例提交位置

**请按场景把样例提交到如下知识体系仓库的目录**

| 样例场景     | Openharmony-SIG样例仓库                                      |
| ------------ | ------------------------------------------------------------ |
| 智能家居场景 | [knowledge_demo_smart_home](https://gitee.com/openharmony-sig/knowledge_demo_smart_home) |
| 影音娱乐场景 | [knowledge_demo_entainment](https://gitee.com/openharmony-sig/knowledge_demo_entainment) |
| 购物消费场景 | [knowledge_demo_shopping](https://gitee.com/openharmony-sig/knowledge_demo_shopping) |
| 运动健康场景 | [knowledge_demo_temp](https://gitee.com/openharmony-sig/knowledge_demo_temp) |
| 智能出行场景 | [knowledge_demo_travel](https://gitee.com/openharmony-sig/knowledge_demo_travel) |
| 智慧办公场景 | [knowledge_demo_temp](https://gitee.com/openharmony-sig/knowledge_demo_temp) |
| 快速上手场景 | [knowledge_demo_temp](https://gitee.com/openharmony-sig/knowledge_demo_temp) |
| 其他场景     | [knowledge_demo_temp](https://gitee.com/openharmony-sig/knowledge_demo_temp) |
| 赛事活动作品 | [online_event](https://gitee.com/openharmony-sig/online_event) |

## 开发样例场景仓库目录结构

为保障开发者很好的使用开发样例仓库，请务必保持各个样例场景仓库的结构不便，不符合目录结构的样例可能不被合入。

下面以智慧家居场景为例描述该仓库的目录结构：

```
knowledge_demo_smart_home/   // 开发样例场景仓库根目录
├── dev   // 用于存储轻/小型设备应用开发样例（C/C++）
│   ├── device  // 用于存储轻/小型设备的设备代码，仅仅托管不在开发板SIG/主干的开发板
│   │   ├── device_x1x
│   │   └── device_x2x
│   ├── team_x  // 样例目录，每个轻/小型开发样例都可以认为是一个产品
│   │   ├── demo_x1x  // 开发样例demo_x1x， 本开发样例相关的代码都放在该目录下
│   │   │   └── demo_x1x_main.c
│   │   └── demo_x2x
│   │       └── demo_x1x_main.c
│   └── third_party  // 轻/小型设备开发样例涉及的不在SIG/主干的三方库（组件）
│       ├── third_component_1  // 三方组件1
│       └── third_component_2
├── docs   // 所有的开发样例的说明文档目录
│   ├── demo_x1x_usage  // 开发样例demo_x1x的说明文档
│   │   ├── media   // 开发样例说明文档涉及到的相关媒体文件（图片动图等）目录
│   │   │   └── architecture.png  
│   │   └── README_zh.md  // 开发样例demo_x1x的主要说明文档
│   └── demo_x2x_usage
│       ├── media
│       │   └── architecture.png
│       └── README_zh.md
├── FA  // 标准设备开发样例工程目录
│   ├── FA_project_1  // 标准设备开发样例工程1
│   └── FA_project_2
├── media // 开发样例场景仓库说明文档涉及的相关媒体文件目录
│   └── flow.png       
└── README_zh.md  // 开发样例场景仓库主说明文档
```

## 开发样例仓库说明模板

定义的开发样例场景仓库主说明文档的模板，各个延伸的场景的说明文档可以按照[开发样例场景仓库说明文档模板](./git_readme_template.md)进行补充说明。

## 开发样例说明模板

定义的开发样例的说明文档模板，共建的各个开发样例可以按照[开发样例说明文档模板](./demo_readme_template.md)进行编写

## 开发样例代码规范

+ 开发样例代码规范参考[OpenHarmony代码规范说明](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.0-LTS/zh-cn/contribute/贡献代码.md)
+ 开发样例代码提交可以参考[开发样例代码提交说明](./demo_code_submit.md)

## 参考资料

+ [开发样例场景仓库说明文档模板](./git_readme_template.md)
+ [开发样例说明文档模板](./demo_readme_template.md)
+ [OpenHarmony代码规范说明](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.0-LTS/zh-cn/contribute/贡献代码.md)
+ [开发样例代码提交说明](./demo_code_submit.md)

