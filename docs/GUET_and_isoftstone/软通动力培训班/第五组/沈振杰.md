# 桂林电子科技大学&软通动力OpenHarmony训练营心得体会

此次通过软通动力的实习，我对Openharmany有了一定的了解，还记得在第一天搭建实验环境的时候我想过退缩，但是还是坚持了下来，
在这两周的学习过程中，通过一个个实验，我对Openharmeny也是产生了浓厚的兴趣，希望以后有机会从事这方面的工作，后续也会自己
了解学习这方面的知识，多实践。

## 实训期待

刚开始的时候，感觉有点难，差点被实验环境劝退，对于后面的实验也是有了很大的期待，不过，老师再多点吧，感觉俩老师被我们一天
天举手问问题就跑的累死了，哈哈。



## 实训过程

实验比较多，点亮LED灯，WIFI及UDP,TCP，温湿度采集，华为云连接等，还没学够就结束了，有点舍不得。实验过程中，编译错误是自己
一个问题一个问题慢慢解决，所以，两周下来，感觉纠错能力及代码编写能力有了很大提高。下面是我印象最深的代码。

E53_IA1.c

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include "cmsis_os2.h"
#include "E53_IA1.h"
#include "wifiiot_errno.h"
#include "wifiiot_gpio.h"
#include "wifiiot_gpio_ex.h"
#include "wifiiot_i2c.h"
#include "wifiiot_i2c_ex.h"

#define WIFI_IOT_IO_FUNC_GPIO_0_I2C1_SDA 6
#define WIFI_IOT_IO_FUNC_GPIO_1_I2C1_SCL 6
#define WIFI_IOT_IO_FUNC_GPIO_8_GPIO 0
#define WIFI_IOT_IO_FUNC_GPIO_14_GPIO 4
#define WIFI_IOT_I2C_IDX_1 1


#define WIFI_IOT_IO_NAME_GPIO_8 8
#define WIFI_IOT_IO_NAME_GPIO_0 0
#define WIFI_IOT_IO_NAME_GPIO_1 1

一遍一遍纠错最后才实现，对代码也是有了更深的理解。



## 未来展望
通过两周的学习，我对Openharmony产生了浓厚兴趣，希望以后工作或这学习中能够接触Openharmony，从事相关工作。

